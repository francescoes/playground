export const createNodeListFragment = (nodeList) => {
  const reducer = (fragment, node) => {
    fragment.appendChild(node);
    return fragment;
  };
  return nodeList.reduce(reducer, document.createDocumentFragment());
};

export const setOffset = (element, offset) => {
  if (!(element instanceof HTMLElement)) {
    console.error(`${element} is not an HTMLElement`);
    return;
  }
  element.style.transform = `translateY(${offset}px)`;
  element.dataset.offset = offset;
};

export const scroll = {
  top: 0,
  calculateDirection(scrollTop) {
    const direction = (scrollTop > this.top) ? 1 : -1;
    this.top = scrollTop <= 0 ? 0 : scrollTop;
    return direction;
  },
};

export const cache = {
  list: [],
  add(items) {
    this.list.push(...items);
  },
  get(item, offset, reverse = false) {
    if (reverse) {
      const list = [...this.list].reverse();
      const index = list.indexOf(item) + 1;
      return list.slice(index, index + offset).reverse();
    }
    const index = this.list.indexOf(item) + 1;
    return this.list[index] ? this.list.slice(index, index + offset) : [];
  },
  remove(item) {
    const index = this.list.indexOf(item);
    const removed = this.list.splice(index, 1);
    return removed;
  },
};

export const queue = (fn, time = 0) => setTimeout(fn, time);
