import { Messages } from './components/messages/messages';
import { MessagesService } from './messages.service';
import { cache, queue, setOffset } from './utils';
import './index.css';
import { InfiniteScroller } from './infinite-scroller';

const createMessagesNode = (messages) => {
  const messagesObject = new Messages(messages);
  const messagesNode = messagesObject.render();
  const messagesNodeList = messagesObject.children;
  cache.add(messagesNodeList);
  return messagesNode;
};

const options = {
  fetch: MessagesService.getMessages,
  url: limit => `//message-list.appspot.com/messages?limit=${limit}`,
  onFetch: ({ messages }) => Array.from(createMessagesNode(messages).children),
  offset: 15,
  onView: 50,
};

MessagesService.getMessages(options.url(options.onView))
  .then(({ messages }) => {
    const messagesNode = createMessagesNode(messages);
    queue(() => {
      document.body.appendChild(messagesNode);
      new InfiniteScroller(messagesNode, options);

      const children = Array.from(messagesNode.children);
      const heights = children.map(m => m.clientHeight);
      children.reduce((offsetTop, message, i) => {
        setOffset(message, offsetTop);
        return offsetTop + heights[i];
      }, 0);
    });
  });
