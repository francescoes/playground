export class MessagesService {
  static getMessages(url) {
    return fetch(`${url}&pageToken=${MessagesService.pageToken}`)
      .then(response => response.json())
      .then((data) => {
        MessagesService.pageToken = data.pageToken;
        return data;
      })
      .catch((err) => {
        console.error('Something went wrong', err);
      });
  }
}
MessagesService.pageToken = '';
