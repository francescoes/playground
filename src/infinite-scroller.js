import {
  cache, scroll, queue, setOffset,
} from './utils';

export class InfiniteScroller {
  constructor(element, {
    offset, onView, fetch, onFetch, url,
  }) {
    this.fetch = fetch;
    this.onFetch = onFetch;
    this.url = url;
    this.element = element;
    this.offset = offset;
    this.onView = onView;

    // do not fire scroll event on resize
    this.resizing = false;

    this.topElement = null;
    this.elements = null;
    this.sentinel = null;
    this.calculateStoresDOM();
    this.addEventListeners();
  }

  addEventListeners() {
    window.addEventListener('resize', this.onResize.bind(this));
    this.element.addEventListener('scroll', this.onScroll.bind(this));
    this.element.addEventListener('deletedItem', this.onItemDeleted.bind(this));
  }

  calculateStoresDOM() {
    this.elements = Array.from(this.element.children);
    this.sentinel = this.boundaries().middle;

    // keep scroll on resize
    this.topElement = {
      node: this.elements[0],
      offset: 0,
    };
  }

  getSortedElements() {
    return [...this.elements].sort((a, b) => {
      const firstId = parseInt(a.dataset.id, 10);
      const secondId = parseInt(b.dataset.id, 10);
      return firstId - secondId;
    });
  }

  recyclableElements(inverse = false) {
    const recyclables = this.getSortedElements();
    return inverse ? recyclables.slice(-this.offset) : recyclables.slice(0, this.offset);
  }

  boundaries() {
    const elements = this.getSortedElements();
    return {
      first: elements[0],
      lower: elements[parseInt(this.onView * 0.25, 10)],
      middle: elements[parseInt(this.onView * 0.5, 10)],
      upper: elements[parseInt(this.onView * 0.75, 10)],
      last: elements[elements.length - 1],
      elements,
    };
  }

  replaceElements(newElements, oldElements, borderElement, direction) {
    const borderOffset = parseInt(borderElement.dataset.offset, 10);
    const replace = (i, offset) => {
      if (i === oldElements.length) {
        return undefined;
      }
      const oldElement = oldElements[i];
      const newElement = newElements[i];
      this.element.replaceChild(newElement, oldElement);
      const newOffset = direction === 1 ? offset : offset - newElement.clientHeight;
      setOffset(newElement, newOffset);
      return replace(i + 1, direction === 1 ? newOffset + newElement.clientHeight : newOffset);
    };
    replace(0, direction === 1 ? borderOffset + borderElement.clientHeight : borderOffset);
    this.calculateStoresDOM();
    this.recycling = false;
  }

  onResize() {
    this.resizing = true;
    const heights = this.elements.map(m => m.clientHeight);
    this.elements.reduce((offset, element, i) => {
      setOffset(element, offset);
      return offset + heights[i];
    }, 0);
    const { scrollTop } = this.element;
    const currentOffset = parseInt(this.topElement.node.dataset.offset, 10);
    this.element.scrollTo(0, scrollTop + (currentOffset - this.topElement.offset));
  }

  onItemDeleted({ target }) {
    const id = parseInt(target.dataset.id, 10);
    const { height } = target.getBoundingClientRect();
    const nextSiblings = this.boundaries().elements.filter((element) => {
      const elementId = parseInt(element.dataset.id, 10);
      return elementId > id;
    });
    nextSiblings.forEach((nextSibling) => {
      const nextSiblingOffset = parseInt(nextSibling.dataset.offset, 10) - height;
      setOffset(nextSibling, nextSiblingOffset);
    });

    const insertAfter = (last, element) => {
      const offset = parseInt(last.dataset.offset, 10) + last.getBoundingClientRect().height;
      setOffset(element, offset);
      this.element.appendChild(element);
      this.calculateStoresDOM();
    };

    queue(() => {
      this.element.removeChild(target);
      cache.remove(target);
      const { last } = this.boundaries();
      const cachedElement = cache.get(last, 1);
      if (cachedElement.length) {
        const [element] = cachedElement;
        insertAfter(last, element);
      } else {
        this.fetch(this.url(1))
          .then(this.onFetch)
          .then((elements) => {
            const [element] = elements;
            insertAfter(last, element);
          });
      }
    });
  }

  onScroll() {
    const { scrollTop } = this.element;

    if (this.recycling) {
      return;
    }

    const offsets = this.elements.map((element) => {
      // const { clientHeight } = element;
      // const offset = parseInt(element.dataset.offset, 10) + clientHeight;
      const offset = parseInt(element.dataset.offset, 10);
      return Math.abs(scrollTop - offset);
    });

    const reducer = ({ offset, index }, o, i) => {
      if (offset < o) {
        return { offset, index };
      }
      return { offset: o, index: i };
    };
    const top = offsets.reduce(reducer, { offset: offsets[0], index: 0 });

    // console.log(this.elements[top.index]);

    this.topElement = Object.assign({}, this.topElement, {
      node: this.elements[top.index],
      offset: parseInt(this.elements[top.index].dataset.offset, 10),
    });

    if (this.resizing) {
      this.resizing = false;
      return;
    }

    const scrollDirection = scroll.calculateDirection(scrollTop);
    if (scrollDirection === 1) {
      this.onScrollDown();
    } else {
      this.onScrollUp();
    }
  }

  onScrollDown() {
    // const { y } = this.sentinel.getBoundingClientRect();
    const { y } = this.sentinel.getBoundingClientRect();
    const { last } = this.boundaries();
    if (y < 0) {
      this.recycling = true;
      const oldElements = this.recyclableElements();
      const cachedElements = cache.get(last, this.offset);
      if (cachedElements.length) {
        console.log('cache down');
        if (cachedElements.length < this.offset) {
          console.log(`cache incomplete, fetching remaining ${this.offset - cachedElements.length} items`);
          this.fetch(this.url(this.offset - cachedElements.length))
            .then(this.onFetch)
            .then((elements) => {
              cachedElements.push(...elements);
              this.replaceElements(cachedElements, oldElements, last, 1);
            });
        } else {
          this.replaceElements(cachedElements, oldElements, last, 1);
        }
      } else {
        console.log('fetch');
        this.fetch(this.url(this.offset))
          .then(this.onFetch)
          .then((elements) => {
            this.replaceElements(elements, oldElements, last, 1);
          });
      }
    }
  }

  onScrollUp() {
    // const { y } = this.sentinel.getBoundingClientRect();
    const { y } = this.upperSentinel.getBoundingClientRect();
    const { first } = this.boundaries();
    // don't recycle if the we reached the top of the list
    if (y > document.body.clientHeight && cache.list[0] !== first) {
      console.log('cache up');
      this.recycling = true;
      const oldElements = this.recyclableElements(true);
      const newElements = cache.get(first, this.offset, true);
      this.replaceElements(newElements.reverse(), oldElements.reverse(), first, -1);
    }
  }
}
