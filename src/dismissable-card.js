export class DismissableCard {
  constructor(element) {
    this.element = element;
    this.boundingRect = null;
    const initialTouches = { x: 0, y: 0 };
    this.start = Object.assign({}, initialTouches);
    this.current = Object.assign({}, initialTouches);
    this.delta = Object.assign({}, initialTouches);
    this.event = new CustomEvent('deletedItem', { bubbles: true, cancelable: true });

    this.onSwipe();
  }

  onSwipe() {
    const style = (styles, element = this.element) => {
      requestAnimationFrame(() => {
        Object.assign(element.style, styles);
      });
    };

    const reset = () => {
      const y = parseInt(this.element.dataset.offset, 10);
      style({
        transform: `translate(0px, ${y}px)`,
        opacity: 1,
      });
    };

    this.element.addEventListener('touchstart', (event) => {
      this.boundingRect = this.element.getBoundingClientRect();
      const x = event.touches[0].pageX;
      const y = event.touches[0].pageY;
      this.start = { x, y };
      this.current = { x, y };
      this.delta = { x: 0, y: 0 };
    }, { passive: true });

    this.element.addEventListener('touchmove', (event) => {
      this.current = {
        x: event.touches[0].pageX,
        y: event.touches[0].pageY,
      };
      this.delta = {
        x: this.current.x - this.start.x,
        y: this.current.y - this.start.y,
      };

      const yOffset = parseInt(this.element.dataset.offset, 10);
      const opacity = 1 - (Math.abs(this.delta.x) / this.boundingRect.width);
      style({
        transform: `translate(${this.delta.x}px, ${yOffset}px)`,
        opacity,
      });
    }, { passive: true });

    this.element.addEventListener('touchend', () => {
      const isOverDeleteThreshold = () => (Math.abs(this.delta.x) > this.boundingRect.width * 0.3);
      if (isOverDeleteThreshold()) {
        const yOffset = parseInt(this.element.dataset.offset, 10);
        style({
          transform: `translate(${this.delta.x}px, ${yOffset}px) scale(0)`,
        });
        this.element.dispatchEvent(this.event);
      } else {
        reset();
      }
    }, { passive: true });
  }
}
