import './author.css';

export class Author {
  constructor(name, photoUrl) {
    this.name = name;
    this.photoUrl = photoUrl;
  }

  render(updated) {
    return `
      <div class="author">
        <figure>
          <img class="author__photo" src="//message-list.appspot.com${this.photoUrl}" />
        </figure>
        <div class="author-info">
          <div class="author-info__name">${this.name}</div>
          <div class="author-info__last-update-message">${updated}</div>
        </div>
      </div>
    `;
  }
}
