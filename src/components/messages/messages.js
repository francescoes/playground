import './messages.css';
import { Message } from '../message/message';
import { createNodeListFragment } from '../../utils';

export class Messages {
  constructor(messages) {
    this.children = messages.map(({
      content, updated, id, author,
    }) => {
      const messageObject = new Message(content, updated, id, author);
      return messageObject.render();
    });
    this.element = document.createElement('ul');
    this.element.classList.add('messages');
  }

  render() {
    const messagesFragment = createNodeListFragment(this.children);
    this.element.appendChild(messagesFragment);
    return this.element;
  }
}
