import { formatDistance } from 'date-fns';
import { Author } from '../author/author';
import { DismissableCard } from '../../dismissable-card';
import './message.css';

export class Message {
  constructor(content, updated, id, { name, photoUrl }) {
    this.content = content;
    this.updated = formatDistance(new Date(), new Date(updated));
    this.author = new Author(name, photoUrl);
    this.element = document.createElement('li');
    this.element.classList.add('message');
    this.element.dataset.id = id;

    // add dismissable behaviour
    new DismissableCard(this.element);
  }

  render() {
    this.element.innerHTML = `
      <div class="message__item">
        <article>
          ${this.author.render(this.updated)}
          <div class="message__content">${this.content}</div>
        </article>
      </div>
    `;
    return this.element;
  }
}
